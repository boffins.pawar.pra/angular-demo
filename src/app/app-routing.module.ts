import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './comp/login/login.component';
import { RegisterComponent } from './comp/register/register.component';
import { DashboardComponent } from './comp/dashboard/dashboard.component';
import { AddNewComponent } from './comp/add-new/add-new.component';

const routes: Routes = [{
  path: '', component: LoginComponent,

}, {
  path: 'register', component: RegisterComponent
},
{
  path: 'dashboard', component: DashboardComponent
},
{
  path: 'add-new', component: AddNewComponent
},
{
  path: '**', component: LoginComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
