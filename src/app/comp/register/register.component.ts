import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  regReq = {
    userName: "",
    password: "",
    mobile: null,
    confirmPassword: ""
  }
  status: boolean

  gotoLogin() {
    this.router.navigateByUrl("/login")
  }


  attemptRegister() {
    if (this.regReq.password != this.regReq.confirmPassword) {
      this.status = false
      return

    } else {
      this.status = true
      localStorage.setItem("user", JSON.stringify(this.regReq))
      this.router.navigateByUrl('/login');

    }

  }




}
