import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-new',
  templateUrl: './add-new.component.html',
  styleUrls: ['./add-new.component.css']
})
export class AddNewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  err: boolean

  gender = [
    { val: "male" },
    { val: "female" }
  ]

  board = [
    { val: "SSC" },
    { val: "CBSC" }
  ]

  cast = [
    { val: "OBC" },
    { val: "OPEN" }
  ]

  medium = [
    { val: "English" },
    { val: "Semi-English" },
    { val: "Marathi" }
  ]

  std = [
    { val: "1" },
    { val: "2" },
    { val: "3" },
    { val: "4" },
    { val: "5" },
    { val: "6" },
    { val: "7" },
    { val: "8" },
    { val: "9" },
    { val: "10" }
  ]



  studDetails = {
    studName: "",
    gender: "",
    std: "",
    addDate: new Date(),
    fees: 0,
    board: "",
    cast: "",
    medium: ""
  }

  attemptAdd() {

    this.validate()
    if (this.err != false) return;

    localStorage.setItem("stud", JSON.stringify(this.studDetails))
    console.log("user input>>", this.studDetails)
    alert("user registered succesfully !")


  }

  validate() {
    this.err = false
    if (!this.studDetails.studName) {
      this.err = true
      alert("Please enter student name")
    }
    else if (!this.studDetails.gender) {
      this.err = true
      alert("Please select gender")
    }
    else if (!this.studDetails.std) {
      this.err = true
      alert("Please select standard")
    }
    else if (!this.studDetails.addDate) {
      this.err = true
      alert("Please select admission date")
    }
    else if (!this.studDetails.fees) {
      this.err = true
      alert("Please enter fees")
    }
    else if (!this.studDetails.board) {
      this.err = true
      alert("Please select board")
    }
    else if (!this.studDetails.cast) {
      this.err = true
      alert("Please select cast")
    }

    else {

      if (!this.studDetails.medium) {
        alert("Please select medium")
        this.err = true;
      }
    }
    return this.err
  }



  // validate() {
  //   this.err = false
  //   if (!this.studDetails.studName) {
  //     this.err = true
  //     alert("Please enter student name")
  //   }
  //   else if (!this.studDetails.gender) {
  //     this.err = true
  //     alert("Please select gender")
  //   }
  //   else if (!this.studDetails.std) {
  //     this.err = true
  //     alert("Please select standard")
  //   }
  //   else if (!this.studDetails.addDate) {
  //     this.err = true
  //     alert("Please select admission date")
  //   }
  //   else if (!this.studDetails.fees) {
  //     this.err = true
  //     alert("Please enter fees")
  //   }
  //   else if (!this.studDetails.board) {
  //     this.err = true
  //     alert("Please select board")
  //   }
  //   else if (!this.studDetails.cast) {
  //     this.err = true
  //     alert("Please select cast")
  //   }
  //   else if (!this.studDetails.medium) {
  //     this.err = true
  //     alert("Please select medium")
  //   } else {
  //     this.err = false
  //   }
  //   return this.err

  // }



}
